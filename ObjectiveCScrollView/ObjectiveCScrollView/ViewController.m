//
//  ViewController.m
//  ObjectiveCScrollView
//
//  Created by Heather Wilcox on 2/8/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UIScrollView* sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [sv setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
    [sv setPagingEnabled:YES];
    [self.view addSubview:sv];
    
    
    UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view1.backgroundColor = [UIColor redColor];
    [sv addSubview:view1];
    
    UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view2.backgroundColor = [UIColor blueColor];
    [sv addSubview:view2];
    
    UIView* view3 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    view3.backgroundColor = [UIColor greenColor];
    [sv addSubview:view3];
    
    UIView* view4 = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height * 2 ,self.view.frame.size.width, self.view.frame.size.height)];
    view4.backgroundColor = [UIColor orangeColor];
    [sv addSubview:view4];
    
    UIView* view5 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    view5.backgroundColor = [UIColor yellowColor];
    [sv addSubview:view5];
    
    UIView* view6 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    view6.backgroundColor = [UIColor purpleColor];
    [sv addSubview:view6];
    
    UIView* view7 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
    view7.backgroundColor = [UIColor magentaColor];
    [sv addSubview:view7];
    
    UIView* view8 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
    view8.backgroundColor = [UIColor blackColor];
    [sv addSubview:view8];
    
    UIView* view9 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view9.backgroundColor = [UIColor brownColor];
    [sv addSubview:view9];
    
    //Red Center Piece
    //UIScrollView* sv2 = [[UIScrollView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.width/2, self.view.frame.size.width, self.view.frame.size.height/2)];
    //[sv2 setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height/2)];
    //sv2.backgroundColor = [UIColor redColor];
    //[sv2 setPagingEnabled:YES];
    //[self.view addSubview:sv2];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
